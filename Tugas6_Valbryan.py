#!/usr/bin/env python
# coding: utf-8

# In[11]:


MORSEDICTIONARY = { 'A':'.-', 'B':'-...', 
                    'C':'-.-.', 'D':'-..', 'E':'.', 
                    'F':'..-.', 'G':'--.', 'H':'....', 
                    'I':'..', 'J':'.---', 'K':'-.-', 
                    'L':'.-..', 'M':'--', 'N':'-.', 
                    'O':'---', 'P':'.--.', 'Q':'--.-', 
                    'R':'.-.', 'S':'...', 'T':'-', 
                    'U':'..-', 'V':'...-', 'W':'.--', 
                    'X':'-..-', 'Y':'-.--', 'Z':'--..', 
                    '1':'.----', '2':'..---', '3':'...--', 
                    '4':'....-', '5':'.....', '6':'-....', 
                    '7':'--...', '8':'---..', '9':'----.', 
                    '0':'-----', ', ':'--..--', '.':'.-.-.-', 
                    '?':'..--..', '/':'-..-.', '-':'-....-', 
                    '(':'-.--.', ')':'-.--.-'} 
def encrypt(message): 
    cipher = '' 
    for letter in message: 
        if letter != ' ': 
            cipher += MORSEDICTIONARY[letter] + ' '
        else: 
            cipher += ' '
  
    return cipher 
  
def decrypt(message): 

    message += ' '
    decipher = '' 
    citext = '' 
    for letter in message: 
  
        if (letter != ' '): 
  
            i = 0
  
            citext += letter 
  
        else: 
            i += 1
  
            if i == 2 : 
  
                decipher += ' '
            else: 
  
                decipher += list(MORSEDICTIONARY.keys())[list(MORSEDICTIONARY
                .values()).index(citext)] 
                citext = '' 
  
    return decipher 
  
def main(): 
    message = ".. .-.. --- ...- . ... .- -- ..- . .-.. ... --- -- ..- -.-. .... "
    result = decrypt(message) 
    print (result) 
  
if __name__ == '__main__': 
    main() 


# In[1]:





# In[5]:


def cek_jawaban(param1, param2):
    if param1.lower() == param2.lower():
        return 1
    else:
        return 0

while True:
    total_jawaban_benar = 0
    
    pertanyaan1 = str(input("1. Siapa Presiden Pertama Indonesia "))
    total_jawaban_benar += cek_jawaban(pertanyaan1, "Soekarno")
    print()
    pertanyaan2 = str(input("2. Siapa nama dosen matematika diskrit "))
    total_jawaban_benar += cek_jawaban(pertanyaan2, "Agung")
    print()
    pertanyaan3 = str(input("3. Siapa nama presiden korut "))
    total_jawaban_benar += cek_jawaban(pertanyaan3, "KimJongUn")
    print()
    pertanyaan4 = str(input("4. Siapa nama dosen linear algebra "))
    total_jawaban_benar += cek_jawaban(pertanyaan4, "Jensen")
    print()
    pertanyaan5 = str(input("5. 1+2? "))
    total_jawaban_benar += cek_jawaban(pertanyaan5, "3")
    print()
    pertanyaan6 = str(input("6. 3+3 "))
    total_jawaban_benar += cek_jawaban(pertanyaan6, "6")
    print()
    pertanyaan7 = str(input("7. 6+6 "))
    total_jawaban_benar += cek_jawaban(pertanyaan7, "12")
    print()
    pertanyaan8 = str(input("8. 12+12 "))
    total_jawaban_benar += cek_jawaban(pertanyaan8, "24")
    print()
    pertanyaan9 = str(input("9. 24+4 "))
    total_jawaban_benar += cek_jawaban(pertanyaan9, "28")
    print()
    pertanyaan10 = str(input("10. 4x4 "))
    total_jawaban_benar += cek_jawaban(pertanyaan10, "16")
    print()
    
    if total_jawaban_benar >= 7:
        print("Kamu menjawab benar {}/10".format(total_jawaban_benar))
        print("Kamu menang!")
    else:
        print("Kamu menjawab benar {}/10".format(total_jawaban_benar))
        print("Kamu kalah!")
    


# In[1]:


from random import randint
t = ["Rock", "Paper", "Scissors"]
computer = t[randint(0,2)]
player = False
while player == False:
    player = input("Rock, Paper, Scissors?")
    if player == computer:
        print("Tie!")
    elif player == "Rock":
        if computer == "Paper":
            print("You lose! Paper beats Rock")
        else:
            print("You win! Rock beats Scissors")
    elif player == "Paper":
        if computer == "Scissors":
            print("You lose! Scissors beats Paper")
        else:
            print("You win! Paper beats Rock")
    elif player == "Scissors":
        if computer == "Rock":
            print("You lose! Rock beats Scissors")
        else:
            print("You win! Scissors beat Paper")
    else:
        print("anda salah memasukan kode (Rock, Paper, Scissors)")
    user = input ("lagi / tidak? ")
    if user == "lagi":
        player = False
        computer = t[randint(0,2)]
    else :
        break


# In[ ]:


def split(word): 
    return [char for char in word] 


word = 'Software'
total_word = len(word)
guess_this = []

word_list = list(split(word))

for x in word:
    guess_this.append('')

z = 6

while z != 0:
    print(" ".join(guess_this))
    user_guess = str(input('Enter a character: '))
    a = 0

    for y in word_list:
        if y.lower() == user_guess.lower():
            guess_this[word_list.index(y)] = y
            a += 1

    if a < 1:
        print("None, try again")
        z -= 1
        print("you have {} more tries".format(z))
    elif "".join(guess_this) == word:
        break

if z == 0:
    print("You lose, sorry")
else:
    print("You WIN !, the word is {}".format(word))


# In[ ]:




