#!/usr/bin/env python
# coding: utf-8

# In[2]:


price = 300
import math 


# In[3]:


print(math.sqrt(price))


# In[4]:


stock_index = "SP500"
stock_index[2:]


# In[5]:


print("The {} is at {} today.".format(stock_index,price))


# In[6]:


stock_info = {'SP500':{'today':300,'yesterday':250},'info':['time',[24,7,365]]}


# In[8]:


stock_info['SP500']['yesterday']


# In[11]:


stock_info['info']


# In[29]:


stock_info['info'][1][2]


# In[56]:


def source_finder(x):
    if ('--')in x:
        return print(x[-6:]) 
    else:
        print('Source not found')


# In[57]:


source_finder("PRICE:345.324:SOURCE--QUANDL")


# In[87]:


def pricefinder(y):
    if ('price') or ('PRICE') in y:
        return True
    else:
        return False
      


# In[88]:


pricefinder("PRICE!")


# In[89]:


pricefinder("DUDE, WHAT IS PRICE!!!")


# In[103]:


pricefinder("the price is 300")


# In[116]:


def count_price(s):
    if ('price') or ('PRICE') in s:
        count = s.count('price')
        count_1 = s.count('Price')
        total_price = count+count_1
        return total_price


# In[117]:


count_price("price price price")


# In[121]:


def avg_price(num):
    sum_num = 0
    for t in num:
        sum_num = sum_num + t           

    avg = sum_num / len(num)
    return avg
    


# In[122]:


avg_price ([3,4,5])


# In[123]:





# In[ ]:




