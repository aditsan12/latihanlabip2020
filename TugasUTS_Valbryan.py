#!/usr/bin/env python
# coding: utf-8

# In[3]:


d = "DJANGO"
print (d[0])
print (d[-1])
print (d[0:4])
print (d[1:4])
print (d[4:6])


# In[4]:


l = [3,7,[1,4,'hello']]
print (l)


# In[5]:


l[2].remove('hello')
l


# In[6]:


l[2].append('goodbye')
l


# In[7]:


d1= {'simplekey':'hello'}
d2= {'k1':{'k2':'hello'}}
d3= {'k1':[{'nestkey':['thisdeep',['hello']]}]}


# In[8]:


d1['simplekey']


# In[9]:


d2['k1']['k2']


# In[10]:


d3['k1'][0]['nestkey'][1]


# In[11]:


mylist = [1,1,1,1,1,2,2,2,2,3,3,3,3]

set(mylist)


# In[16]:


age = 4
name = "Sammy"

print("Hello my dog's name is {} and he is {} years old".format(name,age))


# In[17]:


list1 = [1,1,2,3,1]
list2 = [1,1,2,4,1]
list3 = [1,1,2,1,2,3]


def arrayCheck(nums):
    if 1 in nums and 2 in nums and 3 in nums:
        print('True')
    else:
        print('False')

arrayCheck(list1)
arrayCheck(list2)
arrayCheck(list3)


# In[19]:


def stringBits(word):
    print(word[0::2])

stringBits('Hello')
stringBits('Hi')
stringBits('Heeololeo')


# In[18]:


def end_other(a, b):
    if a.lower() == b[-len(a)::1].lower() or b.lower() == a[-len(b)::1].lower():
        print('True')
    else:
        print('False')

end_other('Hiabc', 'abc')
end_other('AbC', 'HiaBc')
end_other('abc', 'abXabc')


# In[20]:


def double_char(name):
    for x in name:
        print(x*2,end = "")

double_char('The')
print()
double_char('AAbb')
print()
double_char('Hi-There')


# In[21]:


def fix_teen(n):
    if n >= 13 and n <= 19 and n != 15 and n != 16:
        return 0
    else:
        return n


def no_teen_sum(a,b,c):
    total = 0
    total += fix_teen(a)
    total += fix_teen(b)
    total += fix_teen(c)
    print(total)

no_teen_sum(1,2,3)
no_teen_sum(2,13,1)
no_teen_sum(2,1,14)


# In[ ]:


def count_evens(num):
    total_even_numbers = 0
    for x in num:
        if x % 2 == 0:
            total_even_numbers += 1
    print(total_even_numbers)

count_evens([2,1,2,3,4])
count_evens([2,2,0])
count_evens([1,3,5])


# In[1]:


from IPython.display import clear_output

print('WELCOME TO TIC TAC TOE GAMES!')
PlayerOne = str(input('Player 1 please select your avatar (X/O) = '))
if PlayerOne == 'X':
    print ('Player 1 play as X & Player 2 play as O ,, Player 2 MOVE FIRST!! ,, GOODLUCK!')
    
    choices = []

    for x in range (0, 9) :
        choices.append(str(x + 1))

    playerOneTurn = True
    winner = False

    def printBoard() :
        print( '\n _____')
        print( '|' + choices[0] + '|' + choices[1] + '|' + choices[2] + '|')
        print( ' _____')
        print( '|' + choices[3] + '|' + choices[4] + '|' + choices[5] + '|')
        print( ' _____')
        print( '|' + choices[6] + '|' + choices[7] + '|' + choices[8] + '|')
        print( ' _____\n')

    while not winner :
        printBoard()

        if playerOneTurn :
            print( "Player 2:")
        else :
            print( "Player 1:")

        try:
            choice = int(input("Your Choice =  "))
        except:
            print("please enter a valid field (1-9)")
            continue
        if choices[choice - 1] == 'X' or choices [choice-1] == 'O':
            print("This place is taken! please select the empty field!")
            continue

        if playerOneTurn :
            choices[choice - 1] = 'O'
        else :
            choices[choice - 1] = 'X'

        playerOneTurn = not playerOneTurn

        for x in range (0, 3) :
            y = x * 3
            if (choices[y] == choices[(y + 1)] and choices[y] == choices[(y + 2)]) :
                winner = True
                printBoard()
            if (choices[x] == choices[(x + 3)] and choices[x] == choices[(x + 6)]) :
                winner = True
                printBoard()

        if((choices[0] == choices[4] and choices[0] == choices[8]) or 
           (choices[2] == choices[4] and choices[4] == choices[6])) :
            winner = True
            printBoard()
        
    if playerOneTurn == 0:
        print ("Player 2 Wins!")
    else:
        print ("Player 1 Wins!")
        
elif PlayerOne == 'O':
    print ('Player 1 play as O & Player 2 play as X ,, Player 1 MOVE FIRST!! ,, GOODLUCK!')
    
    choices = []

    for x in range (0, 9) :
        choices.append(str(x + 1))

    playerOneTurn = True
    winner = False

    def printBoard() :
        print( '\n _____')
        print( '|' + choices[0] + '|' + choices[1] + '|' + choices[2] + '|')
        print( ' _____')
        print( '|' + choices[3] + '|' + choices[4] + '|' + choices[5] + '|')
        print( ' _____')
        print( '|' + choices[6] + '|' + choices[7] + '|' + choices[8] + '|')
        print( ' _____\n')

    while not winner :
        printBoard()

        if playerOneTurn :
            print( "Player 1:")
        else :
            print( "Player 2:")

        try:
            choice = int(input("Your Choice =  "))
        except:
            print("please enter a valid field (1-9)")
            continue
        if choices[choice - 1] == 'X' or choices [choice-1] == 'O':
            print("This place is taken! please select the empty field!")
            continue

        if playerOneTurn :
            choices[choice - 1] = 'O'
        else :
            choices[choice - 1] = 'X'

        playerOneTurn = not playerOneTurn

        for x in range (0, 3) :
            y = x * 3
            if (choices[y] == choices[(y + 1)] and choices[y] == choices[(y + 2)]) :
                winner = True
                printBoard()
            if (choices[x] == choices[(x + 3)] and choices[x] == choices[(x + 6)]) :
                winner = True
                printBoard()

        if((choices[0] == choices[4] and choices[0] == choices[8]) or 
           (choices[2] == choices[4] and choices[4] == choices[6])) :
            winner = True
            printBoard()
        
    print ("Player " + str(int(playerOneTurn + 1)) + " Wins!")
else:
    print ('WRONG CHOICE! PLEASE RESTART THE GAME! (X/O)')
    exit()


# In[ ]:





# In[ ]:




